package serverdir

import (
    "net/http"
    "io/ioutil"
    "fmt"
    "encoding/json"
    "bytes"
    
)

func Find(server string) (string, error) {

    url := "https://us-central1-server-dir.cloudfunctions.net/find_server"
    //body := `{"server":"` + server + `"}`
    
    ibody , err := json.Marshal( struct {
        A string `json:"server"`
    } {
        server,
    })
    if err!=nil {
        return "",err
    }
    
    client := http.Client{}
    res, err := client.Post(url, "application/json", bytes.NewReader(ibody))
    if err != nil {
        return "",err
    }

    if res.StatusCode != 200 {
        return  "" , fmt.Errorf("find_server returned fail code: (%v) %s", res.StatusCode, http.StatusText(res.StatusCode))
    }

    obody, _ := ioutil.ReadAll(res.Body)

    ip := string(obody)

    return ip, nil
}

func Post(server string, password string) error {

    url := "https://us-central1-server-dir.cloudfunctions.net/set_server"
    //body := `{"server":"` + server + `"}`
    
    ibody , err := json.Marshal( struct {
        A string `json:"server"`
        B string `json:"password"`
    } {
        server,
        password,
    })
    if err!=nil {
        return err
    }
    
    client := http.Client{}
    res, err := client.Post(url, "application/json", bytes.NewReader(ibody))
    if err != nil {
        return err
    }

    if res.StatusCode == 400 {
        return fmt.Errorf("set_server cannot find the server \"%s\"!",server)
    
    } else if res.StatusCode != http.StatusAccepted {
        return  fmt.Errorf("set_server returned fail code: (%v) %s", res.StatusCode, http.StatusText(res.StatusCode))
    }


    return nil

}


/*
func main() {

    st := struct {
        A string `json:"message"`
        B string `json:"password"`
    } {
        "ladgd",
        "passwoddddrd",
    }


    buf := &bytes.Buffer{}
    
    enc := json.NewEncoder(buf)
    
    enc.Encode(st)
    
    log.Printf("value: %s", buf.Bytes())

}
*/
